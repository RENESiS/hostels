module GenerationHelper
  def generate_data(countries_count, regions_count, cities_count, hostels_count)
    for country_index in 1..countries_count
      country = Country.new
      country.name = Faker::Address.country
      country.description = Faker::Lorem.paragraph(2)
      country.save
      for region_index in 1..regions_count
        region = Region.new
        region.name = Faker::Address.state
        region.description = Faker::Lorem.paragraph(2)
        region.country = country
        region.save
        for city_index in 1..cities_count
          city = City.new
          city.name = Faker::Address.city
          city.description = Faker::Lorem.paragraph(2)
          city.region = region
          city.save
          if city_inndex = 1
            region.center = city
            region.save
          end
          for hostel_index in 1..hostels_count
            hostel = Hostel.new
            hostel.name = Faker::Lorem.word
            hostel.description = Faker::Lorem.paragraph(2)
            hostel.distance_to_coast = Faker::Number.positive(50, 3000)
            hostel.distance_to_region_center = Faker::Number.positive(50, 300000)
            hostel.city = city
            hostel.save
          end
        end
      end
    end
  end
end