module ApplicationHelper
  def list_view(model_class, models, attributes)
    render :partial => '/shared/item_list', locals: { model_class: model_class, models: models, attributes: attributes }
  end

  def location_view(model, attributes)
    render :partial => '/shared/location', locals: { model_class: model.class, model: model, attributes: attributes }
  end
end
