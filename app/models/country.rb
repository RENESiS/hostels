class Country < ActiveRecord::Base
  validates :name, :description, presence: true
  has_many :regions
  has_many :cities, through: :regions
  has_many :hostels, through: :cities
end
