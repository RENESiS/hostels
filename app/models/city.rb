class City < ActiveRecord::Base
  belongs_to :region
  has_one :country, through: :region
  has_many :hostels

  validates :name, :description, presence: true

  scope :with_region_and_country, -> { joins(:region, :country).select('cities.*', 'regions.name AS region_name', 'countries.name AS country_name') }
end
