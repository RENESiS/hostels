class Region < ActiveRecord::Base
  belongs_to :country
  belongs_to :center, :class_name => 'City'
  has_many :cities
  has_many :hostels, through: :cities

  validates :name, :description, presence: true

  scope :with_relations, -> { joins(:country, :center).select('regions.*', 'countries.name AS country_name', 'cities.name AS region_center') }
end
