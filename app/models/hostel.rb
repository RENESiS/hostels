class Hostel < ActiveRecord::Base
  belongs_to :city
  has_one :region, through: :city
  validates :name, :description, presence: true
  scope :with_relations, -> { joins(:city, :region).select('hostels.*', 'cities.name AS city_name', 'regions.name AS region_name') }
end
