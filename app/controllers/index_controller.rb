
class IndexController < ApplicationController
  include GenerationHelper

  def index
    @hostels_count = Hostel.count
    @cities_count = City.count
    @regions_count = Region.count
    @countries_count = Country.count
  end

  def about
    add_breadcrumb 'About'
  end

  def generate
    generate_data 2, 10, 4, 3
  end
end
