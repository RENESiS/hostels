class ApplicationController < ActionController::Base
  include ApplicationHelper
  WillPaginate.per_page = 15
  add_breadcrumb 'Главная', :root_path
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
end
