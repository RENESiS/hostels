json.array!(@hostels) do |hostel|
  json.extract! hostel, :id, :name, :description, :distance_to_region_center, :distance_to_coast, :city_id
  json.url hostel_url(hostel, format: :json)
end
